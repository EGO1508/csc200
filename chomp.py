from gasp import * 

the_layout = [
  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%",     # There are 31 '%'s in this line
  "%.....%.................%.....%",
  "%o%%%.%.%%%.%%%%%%%.%%%.%.%%%o%",
  "%.%.....%......%......%.....%.%",
  "%...%%%.%.%%%%.%.%%%%.%.%%%...%",
  "%%%.%...%.%.........%.%...%.%%%",
  "%...%.%%%.%.%%% %%%.%.%%%.%...%",
  "%.%%%.......%GG GG%.......%%%.%",
  "%...%.%%%.%.%%%%%%%.%.%%%.%...%",
  "%%%.%...%.%.........%.%...%.%%%",
  "%...%%%.%.%%%%.%.%%%%.%.%%%...%",
  "%.%.....%......%......%.....%.%",
  "%o%%%.%.%%%.%%%%%%%.%%%.%.%%%o%",
  "%.....%........P........%.....%",
  "%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%"]

GRID_SIZE = 30 
MARGIN = GRID_SIZE 
CHOMP_COLOR = color.YELLOW                   # Put these at top of your program
CHOMP_SIZE = GRID_SIZE * 0.8                 # How big to make Chomp
CHOMP_SPEED = 0.25                

BACKGROUND_COLOR = color.BLACK     
WALL_COLOR = (0.6 * 255, 0.9 * 255, 0.9 * 255)

class Immovable:
    def is_a_wall(self):
        return False  

class Movable:
    def __init__(self, maze, point, speed):
        self.maze = maze                      # For finding other objects
        self.place = point                    # Our current position
        self.speed = speed

    def nearest_grid_point(self):
        (current_x, current_y) = self.place
        grid_x = int(current_x + 0.5)        # Find nearest vertical grid line
        grid_y = int(current_y + 0.5)        # Find nearest horizontal grid line
        return (grid_x, grid_y)

    def furthest_move(self, movement):
        (move_x, move_y) = movement                      # How far to move
        (current_x, current_y) = self.place              # Where are we now?
        nearest = self.nearest_grid_point()              # Where's nearest grid point?
        (nearest_x, nearest_y) = nearest
        maze = self.maze

        if move_x > 0:                                   # Are we moving towards a wall on right?
            next_point = (nearest_x+1, nearest_y)
            if maze.object_at(next_point).is_a_wall():
                if current_x+move_x > nearest_x:         # Are we close enough?
                    move_x = nearest_x - current_x       # Stop just before it

        elif move_x < 0:                                 # Are we moving towards a wall on left?
            next_point = (nearest_x-1, nearest_y)
            if maze.object_at(next_point).is_a_wall():
                if current_x+move_x < nearest_x:         # Are we close enough?
                    move_x = nearest_x - current_x       # Stop just before it

        if move_y > 0:                                   # Are we moving towards a wall above us?
            next_point = (nearest_x, nearest_y+1)
            if maze.object_at(next_point).is_a_wall():
                if current_y+move_y > nearest_y:         # Are we close enough?
                    move_y = nearest_y - current_y       # Stop just before it

        elif move_y < 0:                                 # Are we moving towards a wall below us?
            next_point = (nearest_x, nearest_y-1)
            if maze.object_at(next_point).is_a_wall():
                if current_y+move_y < nearest_y:         # Are we close enough?
                    move_y = nearest_y - current_y       # Stop just before it

        if move_x > self.speed:                          # Don't move further than our speed allows
            move_x = self.speed
        elif move_x < -self.speed:
            move_x = -self.speed

        if move_y > self.speed:
            move_y = self.speed
        elif move_y < -self.speed:
            move_y = -self.speed

        return (move_x, move_y)

    def nearest_grid_point(self):
        (current_x, current_y) = self.place
        grid_x = int(current_x + 0.5)        # Find nearest vertical grid line
        grid_y = int(current_y + 0.5)        # Find nearest horizontal grid line
        return (grid_x, grid_y) 

class Nothing(Immovable):
    pass

class Maze:
    def __init__(self):
        self.have_window = False        # We haven't made window yet
        self.game_over = False          # Game isn't over yet
        self.set_layout(the_layout)     # Make all objects
        set_speed(20)
    
    def set_layout(self, layout):
        height = len(layout)                   # Length of list
        width = len(layout[0])                 # Length of first string
        self.make_window(width, height)
        self.make_map(width, height)           # Start new map

        max_y = height - 1
        for x in range(width):                 # Go through whole layout
            for y in range(height):
                char = layout[max_y - y][x]    # See discussion 1 page ago
                self.make_object((x, y), char)

    def make_window(self, width, height):
        grid_width = (width-1) * GRID_SIZE     # Work out size of window
        grid_height = (height-1) * GRID_SIZE
        screen_width = 2*MARGIN + grid_width
        screen_height = 2*MARGIN + grid_height
        begin_graphics(screen_width,           # Create window
                       screen_height,
                       "Chomp",
                       BACKGROUND_COLOR)
    
    def to_screen(self, point):
        (x, y) = point
        x = x*GRID_SIZE + MARGIN     # Work out coordinates of point
        y = y*GRID_SIZE + MARGIN     # on screen
        return (x, y)
    
    def make_map(self, width, height):
        self.width = width                 # Store size of layout
        self.height = height
        self.map = []                      # Start with empty list
        for y in range(height):
            new_row = []                   # Make new row list
            for x in range(width):
                new_row.append(Nothing())  # Add entry to list
            self.map.append(new_row) 
    
    def make_object(self, point, character):
        (x, y) = point
        if character == '%':                    # Is it a wall?
            self.map[y][x] = Wall(self, point)

    def finished(self):
        return self.game_over        # Stop if game is over

    def play(self):
        update_when('next_tick')     # Just pass time at loop rate

    def done(self):
        end_graphics()               # We've finished
        self.map = []
    def object_at(self, point):
        (x, y) = point

        if y < 0 or y >= self.height:         # If point is outside maze,
            return Nothing()                  # return nothing.

        if x < 0 or x >= self.width:
            return Nothing()

        return self.map[y][x]
    
    def make_object(self, point, character):
        (x, y) = point
        if character == '%':                         # Is it a wall?
            self.map[y][x] = Wall(self, point)
        elif character == 'P':                       # Is it Chomp?
            chomp = Chomp(self, point)
            self.movables.append(chomp)

    def set_layout(self, layout):
        height = len(layout)             # This is as before
        width = len(layout[0])
        self.make_window(width, height)
        self.make_map(width, height)
        self.movables = []               # Start with no Movables

        max_y = height - 1
        for x in range(width):           # Make objects as before
            for y in range(height):
                char = layout[max_y - y][x]
                self.make_object((x, y), char)

        for movable in self.movables:    # Draw all movables
            movable.draw()

    def play(self):
        for movable in self.movables:     # Move each object
            movable.move()
        update_when('next_tick')

    def done(self):
        end_graphics()                   # We've finished
        self.map = []                    # Forget all stationary objects
        self.movables = [] 

class Wall(Immovable):
    def __init__(self, maze, point):
        self.place = point                          # Store our position
        self.screen_point = maze.to_screen(point)
        self.maze = maze                            # Keep hold of Maze
        self.draw() 

    def is_a_wall(self):
        return True 

    def draw(self):
        dot_size = GRID_SIZE * 0.2               # You can remove...
        Circle(self.screen_point, dot_size,      # ...these three lines...
               color = WALL_COLOR, filled = 1.0)   # ...if you like.
        (x, y) = self.place
        # Make list of our neighbors.
        neighbors = [(x + 1, y), (x - 1, y), (x, y + 1), (x, y - 1)]
        # Check each neighbor in turn.
        for neighbor in neighbors:
            self.check_neighbor(neighbor)
    
    def check_neighbor(self, neighbor):
        maze = self.maze
        object = maze.object_at(neighbor)            # Get object

        if object.is_a_wall():                       # Is it a wall?
            here = self.screen_point                 # Draw line from here...
            there = maze.to_screen(neighbor)         # ... to there if it is
            Line(here, there, color=WALL_COLOR, thickness=2)

class Chomp(Movable):
    def __init__(self, maze, point):
        Movable.__init__(self, maze, point,  # Just call the Movable initializer
                         CHOMP_SPEED)

    def move(self):
        keys = keys_pressed()
        if   'left' in keys: self.move_left()   # Is left arrow pressed?
        elif 'right' in keys: self.move_right() # Is right arrow pressed?
        elif 'up' in keys: self.move_up()       # Is up arrow pressed?
        elif 'down' in keys: self.move_down()
    def move_left(self):
        self.try_move((-1, 0))

    def move_right(self):
        self.try_move((1, 0))

    def move_up(self):
        self.try_move((0, 1))

    def move_down(self):
        self.try_move((0, -1))
    
    def try_move(self, move):
        (move_x, move_y) = move
        (current_x, current_y) = self.place
        (nearest_x, nearest_y) = (self.nearest_grid_point())

        if self.furthest_move(move) == (0, 0):         # If we can't move, do nothing.
            return

        if move_x != 0 and current_y != nearest_y:     # If we're moving horizontally
            move_x = 0                                 # but aren't on grid line
            move_y = nearest_y - current_y             # move towards grid line

        elif move_y != 0 and current_x != nearest_x:   # If we're moving vertically
            move_y = 0                                 # but aren't on grid line
            move_x = nearest_x - current_x             # Move towards grid line

        move = self.furthest_move((move_x, move_y))    # Don't go too far
        self.move_by(move)
    
    def draw(self):
        maze = self.maze
        screen_point = maze.to_screen(self.place)
        angle = self.get_angle()                     # Work out half of mouth angle
        endpoints = (self.direction + angle,         # Rotate according to direction
                     self.direction + 360 - angle)
        self.body = Arc(screen_point, CHOMP_SIZE,    # Draw sector
                        endpoints[0], endpoints[1],
                        filled=True, color=CHOMP_COLOR)

    def get_angle(self):
        (x, y) = self.place                                   # Work out distance
        (nearest_x, nearest_y) = (self.nearest_grid_point())  # to nearest grid point
        distance = (abs(x-nearest_x) + abs(y-nearest_y))      # Between -1/2 and 1/2
        return 1 + 90*distance                      
    
    def move_by(self, move):
        self.update_position(move)
        old_body = self.body          # Get old body for removal
        self.draw()                   # Make new body
        remove_from_screen(old_body)  # Remove old body

    def update_position(self, move):
        (old_x, old_y) = self.place                     # Get old coordinates
        (move_x, move_y) = move                         # Unpack vector
        (new_x, new_y) = (old_x+move_x, old_y+move_y)   # Get new coordinates
        self.place = (new_x, new_y)                     # Update coordinates

        if move_x > 0:                   # If we're moving right ...
            self.direction = 0           # ... turn to face right.
        elif move_y > 0:                 # If we're moving up ...
            self.direction = 90          # ... turn to face up.
        elif move_x < 0:                 # If we're moving left ...
            self.direction = 180         # ... turn to face left.
        elif move_y < 0:                 # If we're moving down ...
            self.direction = 270
    




the_maze = Maze()

while not the_maze.finished():
    the_maze.play()

the_maze.done()